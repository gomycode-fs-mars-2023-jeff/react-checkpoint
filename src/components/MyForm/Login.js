import React from 'react';

const Login = () => {
    return (
        <div>
            <section className='main'>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7 col-md-12 d-flex align-items-center">
                            <div className="text text-center">
                                <h1 className='text-white'>Space, our friends!</h1>
                                <h2 className='text-white'>See space like never back then.</h2>
                                <p className='text-white mt-5'>If you share our values and want to have a particular experience, join our team of passionate individuals who are driven to make a difference. </p>
                            </div>
                        </div>
                        <div className="col-lg-5 col-md-10 col-sm-12">
                            <div className="form-box px-5 py-4">
                                <form action="">
                                    <h2 className='text-center mb-4'>Enregistrez-vous !</h2>
                                    <input type="text" name="" placeholder='Nom' className='form-control mb-3' />
                                    <input type="text" name="" placeholder='Email' className='form-control mb-3' />
                                    <select name="" id="" className='form-select mb-3'>
                                        <option value="">Ville</option>
                                        <option value="">Abidjan</option>
                                        <option value="">Yamoussoukro</option>
                                        <option value="">Bonoua</option>
                                    </select>
                                    <input type="text" name="" id="" placeholder='Contact' className='form-control mb-3'/>
                                    <input type="password" name="" id="" placeholder='Mot de passe' className='form-control mb-3'/>
                                    <input type="password" name="" id="" placeholder='Répétez mot de passe' className='form-control mb-3'/>
                                    <div className='mb-3'>
                                        <input type="checkbox" name="" id="" className='check' />
                                        <label htmlFor="conditons"><small className='LowText'>J'accepte les <a href="#" className='link'><b>conditions d'utilisation.</b></a></small></label>
                                    </div>
                                    <button type='submit' className='register-btn form-control mb-3'><b>Creez votre compte</b></button>
                                    <p className='text-center'>Déjà un membre ? <a href="#" className='link'><b>Connectez-vous</b></a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
}

export default Login;
